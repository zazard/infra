#!/usr/bin/env bash

BASEDIR=$(realpath $(dirname "$0"))
HOSTNAME=$1
CA="${BASEDIR}/../keys/ca"

echo "pinging $HOSTNAME"
ping -c 1 -w 5 $HOSTNAME > /dev/null 2> /dev/null || { echo "failed to ping $HOSTNAME"; exit 1; }

TMPDIR=$(mktemp -d)
cd $TMPDIR

echo "getting ssh keys for $HOSTNAME"
ssh-keyscan "${HOSTNAME}" 2>/dev/null | while read hostname keytype key; do short_keytype=$(echo $keytype | cut -d'-' -f2-); echo "processing ${short_keytype} key for ${hostname}"; echo "${keytype} ${key}" > ssh_host_${short_keytype}_key.pub; done || exit 1
echo "signing keys using CA ${CA}"
ssh-keygen -s ${CA} -I beachhead.poneybl.eu -n beachhead,beachhead.poneybl.eu -h *.pub || exit 1
echo "copying new keys to host"
scp -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" *-cert.pub $HOSTNAME:~/ || exit 1
echo "moving keys to /etc/ssh (will ask for sudo password)"
ssh -t -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $HOSTNAME -- sudo mv *-cert.pub /etc/ssh/ || exit 1
echo "cleaning up tmpdir $TMPDIR"
cd -
rm -r $TMPDIR
echo "done"
