{
  pkgs,
  my,
  ...
}: {
  environment.systemPackages = with pkgs; [
    tailscale
    trayscale
  ];
  services.tailscale.enable = true;
  services.tailscale.useRoutingFeatures = "both";
}
