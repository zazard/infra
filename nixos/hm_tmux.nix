{
  programs.tmux = {
    enable = true;
    prefix = "C-a";
    terminal = "xterm-256color";
    aggressiveResize = true;
    clock24 = true;
    historyLimit = 999999999;
    mouse = true;
    extraConfig = ''
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

bind R source-file ~/.tmux.conf \; display-message "  Config reloaded..".
bind Space choose-buffer

bind | split-window -h
bind - split-window -v

bind-key -n S-Left select-pane -L
bind-key -n S-Right select-pane -R
bind-key -n S-Up select-pane -U
bind-key -n S-Down select-pane -D

bind Right next
bind Left prev

bind [ copy-mode

set -g status-bg black
set -g status-fg white
set-option -g status-justify centre
set-option -g status-left '#[fg=green][#[bg=black,fg=cyan]#H#[fg=green]]'
set-option -g status-left-length 20
setw -g automatic-rename on
set-window-option -g window-status-format '#[default]#I#[default]:#[default]#W#[fg=grey,dim]#F'
set-window-option -g window-status-current-format '#[bg=green,fg=black]#I#[bg=green,fg=black]:#[fg=black]#W'
set -g base-index 1
set -g status-right '#[fg=green][#[fg=blue]%Y-%m-%d #[fg=white]%H:%M#[default]  #($HOME/bin/battery)#[fg=green]]'
    '';
  };
}
