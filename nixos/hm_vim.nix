{pkgs, ...}: {
  programs.neovim = {
    enable = true;
    plugins = with pkgs.vimPlugins; [
      # Plugins
      {
        plugin = ansible-vim;
        config = ''
          "ansible-vim
          let g:ansible_name_highlight = 'd'
          let g:ansible_template_syntaxes = {
                      \'*.conf.j2': 'conf',
                      \'*.sh.j2': 'bash',
                      \'*.yml.j2': 'yaml',
                      \'*.yaml.j2': 'yaml',
                      \}
        '';
      }
      indentLine
      python-mode
      {
        plugin = ale;
        config = ''
          let g:ale_fix_on_save = 1
          let g:ale_fixers = {
          \  'nix': ['alejandra'],
          \  'python': ['black'],
          \}
        '';
      }
      vim-go
      vim-hcl
      vim-nix
      vim-terraform

      # Colorschemes
      inkpot
      wombat256
      zenburn
    ];
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;
    withPython3 = true;
    extraConfig = ''
      set t_Co=256
      colorscheme wombat256mod
      filetype off
      filetype plugin indent on

      syntax on;

      set tabstop=4
      set shiftwidth=4
      set noexpandtab

      set nocompatible
      set modelines=1

      set encoding=utf-8
      set scrolloff=3
      set showmode
      set showcmd
      set hidden
      set wildmenu
      set wildmode=list:longest
      set ttyfast
      set ruler
      set number
      set backspace=indent,eol,start
      set smartindent
      set wrap
      set textwidth=79
      set formatoptions=qrnl
      set ignorecase
      set smartcase
      set gdefault
      set incsearch
      set showmatch
      set hlsearch

      set cursorline
      set list
      set lcs=tab:»·,trail:·

      "Automatic setting of xterm titlebar
      set title

      inoremap <F1> <ESC>
      nnoremap <F1> <ESC>
      vnoremap <F1> <ESC>
      nnoremap ; :

      set wmw=0
      set wmh=0
      map <C-Left> <C-W>h<C-W><Bar>
      map <C-Down> <C-W>j<C-W>_
      map <C-Up> <C-W>k<C-W>_
      map <C-Right> <C-W>l<C-W><Bar>
      set sb

      set noeb
      set vb
      set t_vb=

      au BufRead,BufNewFile Vagrantfile set ft=ruby
      au BufRead,BufNewFile */.ssh/config.d/* set ft=sshconfig

      set signcolumn=number
    '';
    extraPackages = with pkgs; [
      alejandra
      (python3.withPackages (ps:
        with ps; [
          black
          flake8
          jedi
          setuptools
        ]))
    ];
  };
}
