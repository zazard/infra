{...}: rec {
  keys = import ./keys.nix;
  modules.imports = [
    ./base.nix
    ./nix.nix
    ./zaz.nix
    ./home-manager.nix
  ];
  qwerty-fr.imports = [./qwerty-fr.nix];
  graphical.imports = [./graphical.nix];
  audio-tools.imports = [./audio-tools.nix];
  lighting-tools.imports = [./lighting-tools.nix];
  tailscale.imports = [./tailscale.nix];
  pkgs = import ./pkgs;
}
