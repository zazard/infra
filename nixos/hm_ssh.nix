{
  services.ssh-agent.enable = true;
  programs.ssh = {
    enable = true;
    matchBlocks = {
      "beachhead" = {
        hostname = "beachhead.poneybl.eu";
        user = "zaz";
      };
      "blackjack" = {
        hostname = "blackjack.poneybl.eu";
        user = "zaz";
      };
    };
  };
}
