{
  programs.bash = {
    enable = true;
    historyControl = ["ignoredups" "ignorespace"];
    historyFileSize = 2000;
    historyIgnore = [
      "ls"
      "l"
      "ll"
      "la"
      "lla"
    ];
    historySize = 1000;
    shellAliases = {
      l = "ls -l";
      lh = "ls -lh";
      la = "ls -la";
      lah = "ls -lah";
      gtr = "git log --all --graph --decorate --oneline";
      gd = "git diff";
      gds = "git diff --staged";
      gst = "git status";
    };
    shellOptions = [
      "histappend"
      "checkwinsize"
    ];
    bashrcExtra = ''
      if [ -x /usr/bin/dircolors ]; then
          test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
          alias ls='ls --color=auto'
          alias grep='grep --color=auto'
          alias fgrep='fgrep --color=auto'
          alias egrep='egrep --color=auto'
      fi
    '';
    initExtra = ''
      PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
      #PS1='\u@\h:\w\$ '

      # If this is an xterm set the title to user@host:dir
      case "$TERM" in
      xterm*|rxvt*)
          PS1="\[\e]0;\u@\h: \w\a\]$PS1"
          ;;
      *)
          ;;
      esac
    '';
  };
  programs.fzf = {
    enable = true;
    enableBashIntegration = true;
    tmux.enableShellIntegration = true;
  };
}
