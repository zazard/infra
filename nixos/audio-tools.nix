{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    audacity
    vcv-rack
    owon-vds1022
    roomeqwizard
    ardour
    opensoundmeter-jack
    bitwig-studio

    yabridge
    yabridgectl

    geonkick
  ];

  # Oscilloscope needs USB perms for the user
  services.udev.extraRules = ''
    # Allow user access to some USB devices.
    SUBSYSTEM=="usb", ATTR{idVendor}=="5345", ATTR{idProduct}=="1234", TAG+="uaccess", RUN{builtin}+="uaccess"
  '';
  musnix.enable = true;
  musnix.kernel.realtime = true;
}
