{my, ...}: {
  users.users.zaz = {
    uid = 1000;
    isNormalUser = true;
    description = "zaz";
    extraGroups = ["networkmanager" "wheel" "mlocate"];
    openssh.authorizedKeys.keys = my.keys.zaz;
  };
}
