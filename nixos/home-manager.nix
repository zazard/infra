{
  pkgs,
  home-manager,
  ...
}: {
  imports = [
    home-manager.nixosModules.home-manager
  ];

  config = {
    home-manager.useGlobalPkgs = true;
    home-manager.useUserPackages = true;

    home-manager.users.zaz = {
      home.username = "zaz";
      home.homeDirectory = "/home/zaz";
      home.stateVersion = "23.11";

      programs.home-manager.enable = true;
      imports = [
        ./hm_bash.nix
        ./hm_git.nix
        ./hm_tmux.nix
        ./hm_vim.nix
        ./hm_ssh.nix
        ./hm_syncthing.nix
      ];
    };
  };
}
