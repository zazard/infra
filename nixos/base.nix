{
  config,
  lib,
  pkgs,
  my,
  ...
}: {
  boot.tmp.cleanOnBoot = true;

  time.timeZone = "Europe/Paris";

  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "fr_FR.UTF-8";
    LC_IDENTIFICATION = "fr_FR.UTF-8";
    LC_MEASUREMENT = "fr_FR.UTF-8";
    LC_MONETARY = "fr_FR.UTF-8";
    LC_NAME = "fr_FR.UTF-8";
    LC_NUMERIC = "fr_FR.UTF-8";
    LC_PAPER = "fr_FR.UTF-8";
    LC_TELEPHONE = "fr_FR.UTF-8";
    LC_TIME = "fr_FR.UTF-8";
  };

  environment.systemPackages = with pkgs; [
    alejandra
    wget
    git
    curlFull
    tmux
    mlocate
    htop
    iotop
    iftop
    p7zip
    nmap
    iperf
    man-pages
    dnsutils
    file
    jq
    pstree
    socat
    tcpdump
    unzip
    usbutils
    pciutils
    lsof
    pv
  ];
  programs.mtr.enable = true;
  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "no";
    hostKeys = [
      {
        bits = 4096;
        path = "/etc/ssh/ssh_host_rsa_key";
        type = "rsa";
      }
      {
        path = "/etc/ssh/ssh_host_ed25519_key";
        type = "ed25519";
      }
    ];
    extraConfig = ''
      HostCertificate /etc/ssh/ssh_host_rsa_key-cert.pub
      HostCertificate /etc/ssh/ssh_host_ed25519_key-cert.pub
    '';
  };

  programs.ssh.knownHostsFiles = [
    (pkgs.writeText "myca.keys" ''
      @cert-authority * ${my.keys.ca}
      @cert-authority * ${my.keys.ca_backup}
    '')
  ];
  networking.firewall.allowedTCPPorts = [
    22
  ];
  services.locate = {
    enable = true;
    package = pkgs.mlocate;
    interval = "hourly";
    localuser = null;
  };
}
