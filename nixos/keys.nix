let
  # This one is going to deprecate soon (GPG key on token)
  zaz_yubi = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBUbgBWBe5yDMLrKuk000wHhEo8zwirgLBC+SUnhizsS cardno:000616833798";
  zaz_testvm = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIANv9wtaBeNgx55vZMxDoC/3VA/Erz10U0QHBGhXTurB zaz@nixos";
  testvm_access = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCHAI8fz3qnXLSFGw0OlDVZD9rFg+KRvCZT2Kvhpk1pJ8Vv6gIv1WFTJWJGIOJmPCMaMwGngq6oDqmvqcm2IesPqbdHWjbCrNZc26yj0Ow3i5vFMlB4FOt34S1QRRKkUshrxNfleILp+K0xChC2EonGyqBuqEjcqy3JYzDhQfYuvXtKwhfhLrcFMU1rq8rgXzr4hJLAjfAl5moLgOxxjemQtya6jRpJ/rRtTFu1rSepaFj8Twoe/4qWL/oQ39znA2hEkW9X46lp9xfV1tuiajnjq/zDl/bgdkqXyTvuGyCYK6BPzKylbspp6BdFSte5DBuV1YUG5JM85EQCNYuFdhFR rsa-key-20240305";
  ca = builtins.readFile ./keys/ca.pub;
  ca_backup = builtins.readFile ./keys/ca_backup.pub;
in {
  zaz = [
    zaz_yubi
    "cert-authority,principals=\"zaz\" ${ca}"
    "cert-authority,principals=\"zaz\" ${ca_backup}"
  ];
  inherit ca;
  inherit ca_backup;
  inherit testvm_access;
}
