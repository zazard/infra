{
  pkgs ? import <nixpkgs> {},
  stdenv ? pkgs.stdenv,
  libusb1 ? pkgs.libusb1,
  libusb-compat ? pkgs.libusb-compat-0_1,
  jdk ? pkgs.jdk17,
  bash ? pkgs.bash,
  makeDesktopItem ? pkgs.makeDesktopItem,
  copyDesktopItems ? pkgs.copyDesktopItems,
}: let
  packageName = "owon-vds1022";
  executable = packageName;
  icon = packageName;
  revision = "1.1.5-cf19";
  humanName = "OWON VDS1022";
in
  stdenv.mkDerivation rec {
    name = packageName;
    version = revision;
    src = pkgs.fetchFromGitHub {
      owner = "florentbr";
      repo = "OWON-VDS1022";
      rev = revision;
      hash = "sha256:06d0l7w9f7rp3aw6lhfwpvrbnm6l3s0wybvyli22kb6prviyd48x";
    };

    buildInputs = [
      libusb1
      libusb-compat
    ];

    nativeBuildInputs = [
      copyDesktopItems
    ];

    desktopItems = [
      (makeDesktopItem {
        name = humanName;
        genericName = humanName;
        desktopName = humanName;
        categories = [
          "Utility"
          "Electronics"
          "Engineering"
        ];
        comment = "Application for the OWON VDS1022 oscilloscope";
        icon = icon;
        exec = executable;
      })
    ];

    installPhase = ''
      mkdir -pv $out/bin $out/lib $out/fwr
      cp -R ${src}/lib/linux/amd64/* $out/lib/
      cp -R ${src}/lib/*.jar $out/lib/
      cp -R ${src}/fwr/* $out/fwr/
      for px in 32 48 64 96 128 256; do
        mkdir -p $out/share/icons/hicolor/''${px}x''${px}/apps
        cp ${src}/ico/icon-''${px}.png $out/share/icons/hicolor/''${px}x''${px}/apps/${icon}.png
      done

      cat > $out/bin/${executable} << EOF
      #!${bash}/bin/bash
      LD_LIBRARY_PATH=${libusb-compat}/lib:${libusb1}/lib ${jdk}/bin/java -cp '$out/lib/*' com.owon.vds.tiny.Main
      EOF
      chmod +x $out/bin/${executable}

      runHook postInstall
    '';

    meta = {
      homepage = "https://github.com/florentbr/OWON-VDS1022";
      description = "Application for the OWON VDS1022 oscilloscope";
      platforms = pkgs.lib.platforms.linux;
      maintainers = [
        {
          name = "zaz";
          email = "nixpkgs@rtljvp.fr";
        }
      ];
    };
  }
