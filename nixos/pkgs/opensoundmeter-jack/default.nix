{
  pkgs ? import <nixpkgs> {},
  stdenv ? pkgs.stdenv,
  fetchFromGitHub ? pkgs.fetchFromGitHub,
  fetchpatch ? pkgs.fetchpatch,
  alsa-lib ? pkgs.alsa-lib,
  jack2 ? pkgs.jack2,
  qt5 ? pkgs.qt5,
}: let
  inherit (qt5) qmake wrapQtAppsHook qtgraphicaleffects qtquickcontrols2;
in
  stdenv.mkDerivation rec {
    pname = "opensoundmeter-jack";
    version = "1.3";

    src = fetchFromGitHub {
      owner = "psmokotnin";
      repo = "osm";
      rev = "v${version}";
      hash = "sha256-nRibcEtG6UUTgn7PhSg4IyahMYi5aSPvaEOrAdx6u3o=";
    };

    patches = [
      ./build.patch
      # Apply PR https://github.com/psmokotnin/osm/pull/60 for JACK support
      (fetchpatch {
        url = "https://patch-diff.githubusercontent.com/raw/psmokotnin/osm/pull/60.patch";
        hash = "sha256-5irsssTjoQQ1abEpCn0DdgZK9NtYE+/iMhmjJdT9S2k=";
      })
    ];

    postPatch = ''
      substituteInPlace OpenSoundMeter.pro \
        --replace 'APP_GIT_VERSION = ?' 'APP_GIT_VERSION = ${src.rev}'
    '';

    nativeBuildInputs = [qmake wrapQtAppsHook];

    qmakeFlags = ["CONFIG+=jack"];

    buildInputs = [jack2 alsa-lib qtgraphicaleffects qtquickcontrols2];

    installPhase = ''
      runHook preInstall

      install OpenSoundMeter -Dt $out/bin
      install OpenSoundMeter.desktop -m444 -Dt $out/share/applications
      install icons/white.png -m444 -D $out/share/icons/OpenSoundMeter.png

      runHook postInstall
    '';

    meta = {
      description = "Sound measurement application for tuning audio systems in real-time - Includes JACK support PR";
      homepage = "https://opensoundmeter.com/";
      license = pkgs.lib.licenses.gpl3Plus;
      mainProgram = "OpenSoundMeter";
      maintainers = [
        {
          name = "zaz";
          email = "nixpkgs@rtljvp.fr";
        }
      ];
      platforms = pkgs.lib.platforms.linux;
    };
  }
