{
  pkgs,
  config,
  lib,
  environment,
  ...
}: {
  services.syncthing = {
    enable = true;
    tray = {
      enable = true;
      package = pkgs.syncthingtray-minimal;
      command = "syncthingtray --wait";
    };
  };
}
