{
  programs.git = {
    enable = true;
    userName = "zaz";
    userEmail = "git@rtljvp.fr";
    aliases = {
      co = "checkout";
      br = "branch";
      ci = "commit";
      st = "status";
      uncommit = "reset --soft HEAD~1";
      unstage = "reset HEAD --";
      last = "log -1 HEAD";
    };
    difftastic.enable = true;
  };
}
