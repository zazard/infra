{
  config,
  pkgs,
  my,
  ...
}: let
  vm_macaddr = "02:00:00:00:00:02";
  vm_ipaddr = "192.168.101.3/24";
  host_ipaddr = "192.168.101.1";
  vm_tapif = "tap3";
  vm_brif = "br0";
in {
  imports = [
    # commented out for VM testing, generate it on actual host laterz ? #./hardware-configuration.nix
    my.modules
    my.tailscale
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "nodev";
  boot.loader.grub.useOSProber = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "clientvm2";

  system.stateVersion = "24.11";

  # test VM stuff
  users.users.zaz.initialPassword = "test";
  networking.firewall.enable = false;
  networking.networkmanager.enable = false;
  networking.useDHCP = false;
  systemd.network = {
    enable = true;
    networks."30-dummy-ethernet" = {
      enable = true;
      matchConfig.MACAddress = vm_macaddr;
      linkConfig = {
        ActivationPolicy = "always-up";
        RequiredForOnline = "yes";
      };
      networkConfig = {
        Address = [vm_ipaddr];
        Gateway = host_ipaddr;
      };
    };
  };
  virtualisation.vmVariant = {
    virtualisation.qemu.networkingOptions = [
      "-netdev tap,id=nd0,ifname=${vm_tapif},script=no,downscript=no,br=${vm_brif}"
      "-device virtio-net-pci,netdev=nd0,mac=${vm_macaddr}"
    ];
  };
}
