{
  config,
  pkgs,
  my,
  ...
}: let
  headscalePort = 34273;
in {
  imports = [
    ./hardware-configuration.nix
    my.modules
    my.tailscale
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "nodev";
  boot.loader.grub.useOSProber = true;

  networking.hostName = "beachhead";

  system.stateVersion = "24.11";

  environment.systemPackages = with pkgs; [
    headscale
  ];
  services.headscale = {
    enable = true;
    address = "0.0.0.0";
    port = headscalePort;
    settings = {
      dns.base_domain = "poneybl.eu";
      tls_letsencrypt_challenge_type = "HTTP-01";
      tls_letsencrypt_hostname = "beachhead.poneybl.eu";
      tls_letsencrypt_listen = ":http";
      server_url = "https://beachhead.poneybl.eu:${toString headscalePort}";
      prefixes.v4 = "100.64.0.0/16";
    };
  };
  networking.firewall.allowedTCPPorts = [
    headscalePort
    80
  ];
  systemd.services.headscale.serviceConfig = {
    AmbientCapabilities = "cap_net_bind_service";
    CapabilityBoundingSet = "cap_net_bind_service";
  };
  boot.kernel.sysctl."net.ipv4.ip_forward" = "1";
  boot.kernel.sysctl."net.ipv6.conf.all.forwarding" = "1";
  networking.nftables.enable = true;
}
