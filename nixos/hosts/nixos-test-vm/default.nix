{
  config,
  pkgs,
  my,
  graphical ? false,
  ...
}: {
  imports =
    [
      ./hardware-configuration.nix
      my.modules
    ]
    ++ (
      if graphical
      then [my.graphical]
      else []
    );

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.useOSProber = true;

  networking.hostName = "nixos-test";
  networking.networkmanager.enable = true;
  programs.nm-applet.enable = true;

  users.users.zaz.initialPassword = "zaz";
}
