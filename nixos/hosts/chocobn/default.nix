{
  config,
  pkgs,
  my,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
    my.modules
    my.graphical
    my.audio-tools
    my.lighting-tools
    my.tailscale
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "nodev";
  boot.loader.grub.useOSProber = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "chocobn";
  networking.networkmanager.enable = true;
  programs.nm-applet.enable = true;

  system.stateVersion = "23.11";

  programs.steam.enable = true;
  environment.systemPackages = with pkgs; [
    protonup-qt
    qemu
  ];

  services.blueman.enable = true;
  # vm host stuff

  networking.networkmanager.unmanaged = ["tap0" "br0"];
  systemd.services."systemd-networkd".environment.SYSTEMD_LOG_LEVEL = "debug";
  systemd.network = {
    enable = true;
    wait-online.enable = false;
    netdevs = {
      "20-tap2" = {
        enable = true;
        netdevConfig = {
          Kind = "tap";
          Name = "tap2";
        };
      };
      "20-tap3" = {
        enable = true;
        netdevConfig = {
          Kind = "tap";
          Name = "tap3";
        };
      };
      "20-tap4" = {
        enable = true;
        netdevConfig = {
          Kind = "tap";
          Name = "tap4";
        };
      };
      "20-bridge0" = {
        enable = true;
        netdevConfig = {
          Kind = "bridge";
          Name = "br0";
        };
      };
    };
    networks = {
      "30-ethernet" = {
        matchConfig.Name = "enp0s31f6";
        linkConfig = {
          Unmanaged = "yes";
        };
      };
      "30-wifi" = {
        matchConfig.Name = "wlp61s0";
        linkConfig = {
          Unmanaged = "yes";
        };
      };
      "40-tap2" = {
        matchConfig.Name = "tap2";
        bridgeConfig = {};
        linkConfig = {
          ActivationPolicy = "always-up";
          RequiredForOnline = "no";
        };
        networkConfig = {
          Bridge = "br0";
        };
      };
      "40-tap3" = {
        matchConfig.Name = "tap3";
        bridgeConfig = {};
        linkConfig = {
          ActivationPolicy = "always-up";
          RequiredForOnline = "no";
        };
        networkConfig = {
          Bridge = "br0";
        };
      };
      "40-tap4" = {
        matchConfig.Name = "tap4";
        bridgeConfig = {};
        linkConfig = {
          ActivationPolicy = "always-up";
          RequiredForOnline = "no";
        };
        networkConfig = {
          Bridge = "br0";
        };
      };
      "40-bridge0" = {
        matchConfig.Name = "br0";
        linkConfig = {
          ActivationPolicy = "always-up";
          RequiredForOnline = "no";
        };
        networkConfig = {
          Address = ["192.168.101.1/24"];
        };
      };
    };
  };
  networking.nat = {
    enable = true;
    enableIPv6 = false;
    externalInterface = "wlp61s0";
    internalInterfaces = ["br0"];
  };
}
