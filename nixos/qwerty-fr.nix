{...}: {
  services.xserver.xkb = {
    layout = "qwerty-fr";
    extraLayouts.qwerty-fr = {
      description = "French qwerty keyboard";
      languages = ["eng" "fr"];
      symbolsFile = builtins.fetchurl {
        url = "https://raw.githubusercontent.com/qwerty-fr/qwerty-fr/master/linux/us_qwerty-fr";
        sha256 = "cf629caa55634dcba32bc8cf615d35b009f163eff088dc92cd27876bef94b433";
      };
    };
  };
}
