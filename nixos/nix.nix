{my, ...}: {
  nixpkgs.config.allowUnfree = true;
  nix.settings.experimental-features = ["nix-command" "flakes"];
  nixpkgs.overlays = [my.pkgs];
  nix.settings.trusted-users = ["zaz"];
}
