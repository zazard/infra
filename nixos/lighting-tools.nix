{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    qlcplus
  ];

  services.udev.extraRules = ''
    # Allow user access to Electroconcept USBDMX
    SUBSYSTEM=="usb", ATTR{idVendor}=="0403", ATTR{idProduct}=="6001", TAG+="uaccess", RUN{builtin}+="uaccess"
    # Allow user access to Behringer BCF2000
    SUBSYSTEM=="usb", ATTR{idVendor}=="1397", ATTR{idProduct}=="00bc", TAG+="uaccess", RUN{builtin}+="uaccess"
  '';
}
