# Infra

```
< I am a newbie nix infra repo >
 ------------------------------
\                             .       .
 \                           / `.   .' "
  \                  .---.  <    > <    >  .---.
   \                 |    \  \ - ~ ~ - /  /    |
         _____          ..-~             ~-..-~
        |     |   \~~~\.'                    `./~~~/
       ---------   \__/                        \__/
      .'  O    \     /               /       \  "
     (_____,    `._.'               |         }  \/~~~/
      `----.          /       }     |        /    \__/
            `-.      |       /      |       /      `. ,~~|
                ~-.__|      /_ - ~ ^|      /- _      `..-'
                     |     /        |     /     ~-.     `-. _  _  _
                     |_____|        |_____|         ~ - . _ _ _ _ _>
```

# Basics

```
$ nixos-rebuild --flake .#HOST_IDENTIFIER switch
```

# Upping a new host

After install it is necessary to bless the SSH host keys using the normal
operation CA, which has its PK in `nixos/keys/ca` (Security token needed to
actually use it, of course).

Get the host keys from the host, then sign them using the `bless_hostkeys.sh`
script in `nixos/scripts`

# Testing in a VM
```
zaz@chocobn:~/infra$ nixos-rebuild --flake .#blackjack build-vm
zaz@chocobn:~/infra$ QEMU_NET_OPTS="hostfwd=tcp::5555-:22" ./result/bin/run-*
```
