{
  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-unstable;
  inputs.home-manager.url = github:nix-community/home-manager;
  inputs.musnix.url = github:musnix/musnix;

  outputs = {
    self,
    nixpkgs,
    home-manager,
    musnix,
    ...
  } @ attrs: let
    my = import ./nixos {};
  in {
    nixosConfigurations.nixos-test-vm = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs =
        attrs
        // {
          inherit my;
          graphical = true;
        };
      modules = [./nixos/hosts/nixos-test-vm];
    };
    nixosConfigurations.chocobn = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs =
        attrs
        // {
          inherit my;
        };
      modules = [
        musnix.nixosModules.musnix
        ./nixos/hosts/chocobn
      ];
    };
    nixosConfigurations.blackjack = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs =
        attrs
        // {
          inherit my;
        };
      modules = [./nixos/hosts/blackjack];
    };
    nixosConfigurations.beachhead = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs =
        attrs
        // {
          inherit my;
        };
      modules = [./nixos/hosts/beachhead];
    };
    nixosConfigurations.clientvm1 = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs =
        attrs
        // {
          inherit my;
        };
      modules = [./nixos/hosts/clientvm1];
    };
    nixosConfigurations.clientvm2 = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs =
        attrs
        // {
          inherit my;
        };
      modules = [./nixos/hosts/clientvm2];
    };
  };
}
